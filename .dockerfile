FROM node:14.17.3

WORKDIR /app

COPY . /app
COPY ./package.json /app
COPY yarn.lock /app

RUN yarn global add node-gyp
RUN yarn install
RUN yarn build

CMD [ "yarn","run","start:prod" ]
